<html lang="fr">
<head>

<meta charset="UTF-8" href="style.css">
<title>Site de communication du lycée Pierre Poivre</title>
<link rel="stylesheet"    type="text/css"    href="style.css">
</head>

<body>
    <div class="container">

<header>
<?php include ("session.php"); ?>
<?php include ("static/entete.php"); ?>
<?php include ("static/search.php"); ?>
</header>

<nav>
<?php include ("static/menu.php"); ?>
</nav>

<article>
<h1>Mes rendez-vous</h1>
<i>Légende : </i><img class="etat_rdv" src="img/refus.png" alt="etat_rdv" width="2%"><strong> REFUSER / <img class="etat_rdv" src="img/valid.png" alt="etat_rdv" width="2%"> ACCEPTER / <img class="etat_rdv" src="img/wait.png" alt="etat_rdv" width="2%"> EN ATTENTE </strong><br/><br>
<br><br><?php 
include("bdd.php");
if(!isset($_SESSION['id']))
{
    echo"Vous n'avez pas les droits d'accéder à cette page, veuillez vous connecter ou si vous ne possèdez pas de compte, merci d'en crée un <a href=\"inscription.php\"> ici </a>";
}
else
{
    $statut = $_SESSION['statut'];
    $id = $_SESSION['id'];
    if($statut == "Etudiant")
    {
        $request = "SELECT * FROM rendez_vous WHERE idMembreEtudiant = '$id' ORDER BY dateRDV DESC";
        $execute = mysqli_query($cnx, $request);
        
        echo"<table><tr><td>Objet</td><td>Professeur concerné</td><td>Date du RDV</td><td></td></tr>";

    
        while($result = mysqli_fetch_array($execute))
        {
            if($result['situationRDV'] == "REFUSER")
                $icon = "refus.png";
            elseif($result['situationRDV'] == "ACCEPTER")
                $icon = "valid.png";
            else
                $icon = "wait.png";
            
            $idresult = $result['idMembreProfesseur'];
            $requestid = "SELECT nomMembre, prenomMembre FROM membre,rendez_vous WHERE membre.idMembre = rendez_vous.idMembreProfesseur AND idMembreProfesseur = '$idresult'";
            $execution = mysqli_query($cnx, $requestid);
            $resultat = mysqli_fetch_array($execution);
            echo'<tr><td><a href="msg_rdv.php?id='.$result['idRDV'].'"><strong>'.$result['objetRDV'].'</strong></a></td><td>'.$resultat['nomMembre'].' '.$resultat['prenomMembre'].' </td><td>'.$result['dateRDV'].'</td><td><img class="etat_rdv" src="img/'.$icon.'" alt="etat_rdv" width="5%"></td>';
        }

       unset($result);

        echo"</tr></table><br /><form action=\"prise_rdv.php\"><input type=\"submit\" value=\"Prendre un RDV\"></form>";
    }
    elseif($statut == "Professeur")
    {
        $request = "SELECT * FROM rendez_vous WHERE idMembreProfesseur = '$id' ORDER BY dateRDV DESC";
        $execute = mysqli_query($cnx, $request);

        echo"<table><tr><td>Objet</td><td>Etudiant concerné</td><td>Date du RDV</td><td></td></tr>";

        while($result=mysqli_fetch_array($execute))
        {
            if($result['situationRDV'] == "REFUSER")
                $icon = "refus.png";
            elseif($result['situationRDV'] == "ACCEPTER")
                $icon = "valid.png";
            else
                $icon = "wait.png";

            $idresult = $result['idMembreEtudiant'];
            $requestid = "SELECT nomMembre, prenomMembre FROM membre,rendez_vous WHERE membre.idMembre = rendez_vous.idMembreEtudiant AND idMembreEtudiant = '$idresult'";
            $resultat = mysqli_fetch_array(mysqli_query($cnx,$requestid));
            echo'<tr><td><a href="msg_rdv.php?id='.$result['idRDV'].'"><strong>'.$result['objetRDV'].'</strong></a></td><td>'.$resultat['nomMembre'].' '.$resultat['prenomMembre'].' </td><td>'.$result['dateRDV'].'</td><td><img class="etat_rdv" src="img/'.$icon.'" alt="etat_rdv" width="5%"></td>';
        }     
        

        unset($result);
        echo'</tr></table>';
    }
    else
    {
        echo'Une erreur s\'est produit';
    }
    
    mysqli_close($cnx);
}
?><br/>
</article>

<footer>
<?php include("static/footer.php"); ?>
</footer>