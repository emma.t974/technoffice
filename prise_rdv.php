<html lang="fr">

<head>

<meta charset="UTF-8" href="style.css">
<title>Site de communication du lycée Pierre Poivre</title>
<link rel="stylesheet"    type="text/css"    href="style.css">
</head>

<script>

function verifDate(champ){
    var date = new Date();
    if(champ.value > date)
        return false;
    else
        return true;
}
function msgDate(f){

    var msgdateok = verifDate(f.Denvoi);

    if(msgdateok)
        return true;
    
    else
    {
        alert('Vous devrez choisir une date valide');
        return false;
    }
}
</script>

<body>
    <div class="container">

<header>
<?php include("session.php"); ?>
<?php include ("static/entete.php"); ?>
<?php include ("static/search.php"); ?>
</header>

<nav>
<?php include("static/menu.php"); ?>
</nav>


<article>
<?php
if($_SESSION['statut'] == "Etudiant")
{
  ?>
<form action="conf-rdv.php" method="post" onsubmit="return msgDate(this)">
    <p> <label for="prof">Professeur:</label>
    	<?php
      include("bdd.php");
  // Variable qui ajoutera l'attribut selected de la liste déroulante
  $selected = '';
 
    // requête pour récuperer les professeurs 
  
  $requete = "SELECT * FROM membre WHERE statutMembre='Professeur'";
    $execution = mysqli_query($cnx,$requete);
    
    // Parcours du tableau
  echo '<select name="Professeur">',"\n";
  
  while($resultat = mysqli_fetch_array($execution))
  {
    //Affichage de la ligne
    $id=$resultat['idMembre'];
    $nom=$resultat['nomMembre'];
    $prenom=$resultat['prenomMembre'];
    echo '<option value='.$id.'>'.$nom.' '.$prenom.'</option>';
}
  echo '</select>',"\n";
  mysqli_close($cnx);
?>
<br>
        <label for="Objet">Objet : </label> <input type="text" name="objet" id="objet" required>
        <br><label for="Message">Message :</label>
        <textarea id="Message" name="Message" rows="5" cols="35" maxlength="1000" required></textarea> <br>

        <label for="Date_envoi">Date :</label>
        <input type="date" id="Denvoi" name="Denvoi" onblur="verifDate(this)" required><br/>
    	<input type="submit" value="Envoyer"/><input type="reset">
</p></form>
<?php
}
else
{
  echo'<meta http-equiv="refresh" content="3; URL=index.php">';
  echo'Vous n\'avez pas accès à cette page, vous serez rediriger automatiquement vers la page d\'accueil';
}
?>
</article>

<footer>
<?php include("static/footer.php"); ?>
</footer>

</html>

