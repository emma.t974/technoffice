<html lang="fr">
<head>

<meta charset="UTF-8" href="style.css">
<title>Site de communication du lycée Pierre Poivre</title>
<link rel="stylesheet"    type="text/css"    href="style.css">
</head>

<body>
    <div class="container">

<header>
<?php include ("session.php"); ?>
<?php include ("static/entete.php"); ?>
<?php include ("static/search.php"); ?>
</header>

<nav>
<?php include ("static/menu.php"); ?>
</nav>

<article>
<h1>Mes rendez-vous</h1>
<?php 
include("bdd.php");
if(!isset($_SESSION['id']))
{
    echo"Vous n'avez pas les droits d'accéder à cette page, veuillez vous connecter ou si vous ne possèdez pas de compte, merci d'en crée un <a href=\"inscription.php\"> ici </a>";
}
else
{
    $id = $_GET['id'];
    $request = "SELECT * FROM rendez_vous WHERE idRDV = '$id'";
    $result = mysqli_fetch_array(mysqli_query($cnx, $request));
    
    if($_SESSION['statut'] == "Professeur")
        $idconcerne = $result['idMembreEtudiant'];
    elseif($_SESSION['statut'] == "Etudiant")
        $idconcerne = $result['idMembreProfesseur'];
    
    $requestprof = "SELECT nomMembre, prenomMembre FROM membre WHERE idMembre ='$idconcerne'";
    $resultprof = mysqli_fetch_array(mysqli_query($cnx,$requestprof));


    echo'Personne concerné : <strong> '.$resultprof['nomMembre'].' '.$resultprof['prenomMembre'].' </strong> <br />';
    echo'Date du rendez-vous : <strong>'.$result['dateRDV'].' </strong> <br>Situation rendez vous : <strong> '.$result['situationRDV'].' </strong><br >'; 
    echo'Objet : <strong> '.$result['objetRDV'].' </strong><br />';
    echo'<br />Message de l\'étudiant : <br \> '.$result['descriptionRDV'].' <br />';

    if($result['reponseRDV'] != NULL)
        echo'<br /> Réponse du professseur : <br \> '.$result['reponseRDV'].'<br />';
    else
    {
        if($_SESSION['statut'] == "Professeur" && $result['situationRDV'] == "En attente")
        {
        echo'<br />Réponse du professeur : <br /><strong>Vous n\'avez pas encore répondu </strong>';
        echo'<br /><br><a href="reponse_RDV.php?id='.$id.'">Répondre au rendez-vous</a>';
        }
        else
        echo'<br /> Votre professeur n\'a pas encore répondu';
    }

    unset($result);
}
?>
    
</article>

<footer>
<?php include("static/footer.php"); ?>
</footer>
