<!DOCTYPE HTML>
<html lang="fr">
<head>
    <title>Condition d'utilisation</title>
</head>
<body>
    <h1>Les conditions d'utilisation</h1><br>
    <p>
    L'inscription permet à l'utiliseur d'utilisé notre service de gestion de rendez-vous. <br>
    En vous inscrivant vous acceptez la prise d'information de vos données personnel. <br>
    L'adminisatrateur du site peut se reservé de supprimer un rendez-vous ou un le compte de l'utilisateur en cas d'abus.<br><br>
    En utilisant notre service, <strong>vous êtes tenu de :</strong> <br>
    <ul>
        <li>De ne pas poster des propos dites diffamatoire,antisémie, raciste, faciste, révisionniste,abusif,vulgaire</li>
        <li>De rester poli envers les autres utilisateurs du site.</li>
        <li>D'éviter tout forme de spam.</li>
    </ul>

    En cas de non respect de ces règles, <strong>le compte de l'utilisateur sera suspendu.</strong><br>
    </p>
</body>
</html>