<html lang="fr">

<head>

<meta charset="UTF-8" href="style.css">
<title>Site de communication du lycée Pierre Poivre</title>
<!-- Icone du site 
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" /> -->
<link rel="stylesheet"    type="text/css"    href="style.css">
</head>

<body>
    <div class="container">

<header>
<?php include ("session.php"); ?>
<?php include ("static/entete.php"); ?>
<?php include ("static/search.php"); ?>
</header>

<nav>
<?php include ("static/menu.php"); ?>
</nav>

<article>
<h1>Mon compte </h1>
<?php
$id = isset($_SESSION['id']);
if(isset($_SESSION['id']))
{
    include ("bdd.php");
    $requete = "SELECT * FROM membre WHERE idMembre='$id'";
    $execution = mysqli_query($cnx,$requete);
    $resultat = mysqli_fetch_array($execution);
    echo'<form action="/validmoncompte.php?id='.$resultat['idMembre'].'" method="POST"  onSubmit="return validation(this)"  enctype="multipart/form-data">
<p> 
<table>
<tr><td>Adresse E-mail :</td><td> '.$_SESSION['login'].' </td></tr>
<tr><td>Votre statut : </td><td> '.$_SESSION['statut'].' </td></tr>
<tr><td>Mot de passe : </td><td><input type="password" name="mdp" id="mdp"> </td></tr>
<tr><td>Confirmer mot de passe : </td><td><input type="password" name="configpassword" id="configpassword"> </td></tr>
</table>
<input type="submit" value="Envoyer"><input type="reset"><br>
<a href="deconnexion.php"> Me deconnecter </a>
</p>
</form>';

unset($resultat);
mysqli_close($cnx);
}
else
{
    echo'<meta http-equiv="refresh" content="3; URL=connexion.php">'; //Redirection automatique
    echo'Vous n\'est pas connecté, vous allez rediger vers la page de connexion';
}
?>

</article>

</div>

<footer>
<?php include("static/footer.php"); ?>
</footer>
</html>
