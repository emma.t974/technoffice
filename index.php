<html lang="fr">

<head>

<meta charset="UTF-8" href="style.css">
<title>Site de communication du lycée Pierre Poivre</title>
<!-- Icone du site 
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" /> -->
<link rel="stylesheet"    type="text/css"    href="style.css">
</head>

<body>
    <div class="container">

<header>
<?php include ("session.php"); ?>
<?php include ("static/entete.php"); ?>
<?php include ("static/search.php"); ?>
</header>

<nav>
<?php include("static/menu.php"); ?>
</nav>

<article>

<p> Bonjour, bienvenue sur le site de tech'n-office, n°1 du site de communication élève à professeur.<br>
	Veuillez vous crée un compte si vous n'êtes pas enregistrer, afin de pouvoir utiliser cette outil!<br>
</p>

</article>

<footer>
<?php include("static/footer.php"); ?>
</footer>
</div>

</html>
