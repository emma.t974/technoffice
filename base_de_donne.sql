-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `membre`;
CREATE TABLE `membre` (
  `idMembre` int(11) NOT NULL AUTO_INCREMENT,
  `emailMembre` varchar(30) NOT NULL,
  `passwordMembre` varchar(50) NOT NULL,
  `nomMembre` varchar(30) NOT NULL,
  `prenomMembre` varchar(30) NOT NULL,
  `statutMembre` varchar(30) NOT NULL,
  `avatarMembre` varchar(255) NOT NULL,
  PRIMARY KEY (`idMembre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `rendez_vous`;
CREATE TABLE `rendez_vous` (
  `idRDV` int(11) NOT NULL AUTO_INCREMENT,
  `dateRDV` date NOT NULL,
  `objetRDV` varchar(255) DEFAULT NULL,
  `descriptionRDV` text,
  `reponseRDV` varchar(255) DEFAULT NULL,
  `fichierJointRDV` varchar(255) DEFAULT NULL,
  `idMembreEtudiant` int(11) NOT NULL,
  `idMembreProfesseur` int(11) NOT NULL,
  `situationRDV` varchar(30) NOT NULL,
  PRIMARY KEY (`idRDV`),
  KEY `idMembreEtudiant` (`idMembreEtudiant`),
  KEY `idMembreProfesseur` (`idMembreProfesseur`),
  CONSTRAINT `rendez_vous_ibfk_1` FOREIGN KEY (`idMembreEtudiant`) REFERENCES `membre` (`idMembre`),
  CONSTRAINT `rendez_vous_ibfk_2` FOREIGN KEY (`idMembreProfesseur`) REFERENCES `membre` (`idMembre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



-- 2018-04-23 10:11:35
