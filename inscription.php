<html lang="fr">

<head>

<meta charset="UTF-8" href="style.css">
<title>Site de communication du lycée Pierre Poivre</title>
<!-- Icone du site 
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" /> -->
<link rel="stylesheet"    type="text/css"    href="style.css">
</head>

<body>
    <div class="container">

<header>
<?php include ("session.php"); ?>
<?php include ("static/entete.php"); ?>
<?php include ("static/search.php"); ?>
</header>

<nav>
<?php include("static/menu.php"); ?>
</nav>

<article>
<script language="JavaScript">
function validation(f) {
  if (f.password.value == '' || f.password2.value == '') {
    alert('Tous les champs ne sont pas remplis');
    f.password.focus();
    return false;
    }
  else if (f.password.value != f.password2.value) {
    alert('Ce ne sont pas les mêmes mots de passe!');
    f.password.focus();
    return false;
    }
  else if (f.passsword.value == f.password2.value) {
    return true;
    }
  else {
    f.password.focus();
    return false;
    }
  }
</script>
<h1>INSCRIPTION</h1>
<?php
if(isset($_SESSION['id']))
{
echo'Vous etes déjà connecté en tant que '.$_SESSION['login'].' vous allez être rediger vers la page d\'accueil';
echo'<meta http-equiv="refresh" content="3; URL=index.php">'; //Redirection automatique
}
else
{
echo'<form action="valid_regist.php" method="POST" onSubmit="return validation(this)"  enctype="multipart/form-data">
<p>
<table>
<tr><td>Prénom : </td><td><input type="text" name="prenom" id="prenom" required></td></tr>
<tr><td>Nom : </td><td><input type="text" name="nom" id="nom" required></td></tr>   
<tr><td>Adresse-Mail : </td><td> <input type="email" name="email" id="email" required></td></tr>
<tr><td>Mot de passe : </td><td><input minlength="8" maxlength="20" type="password" name="password" id="password" required> </td></tr>
<tr><td>Confirmation (mdp) :</td><td><input type="password" name="password2" id="password2" required> </td></tr>
<tr><td>Votre Statut : </td><td><select name="statut" id="statut"><option value="Etudiant">Etudiant</option><option value="Professeur">Professeur</option></select></td></tr>
<tr><td><input type="checkbox" id="condition" name="condition" required></td><td><a href="#" onclick="open("condition.php", "Popup", "scrollbars=1,resizable=1,height=560,width=770"); return false;">J\'accepte les conditions d\'utilisation</a></td></tr>
</table>
<input type="submit" value="M\'inscrire">
</p>
</form>';
}
?>
</article>

</div>

<footer>
<?php include("static/footer.php"); ?>
</footer>
</html>
